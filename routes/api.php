<?php

use Illuminate\Http\Request;
use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
* Snippet for a quick route reference
*/
Route::get('/', function (Router $router) {
    return collect($router->getRoutes()->getRoutesByMethod()["GET"])->map(function($value, $key) {
        return url($key);
    })->values();   
});



Route::group(['middleware' => 'auth:web'], function(){
    Route::get('links','Api\LinksAPIController@index');
    Route::get('links/{links}','Api\LinksAPIController@show');

    Route::resource('links', 'Api\LinksAPIController', [
        'only' => ['store', 'destroy']
    ]);
    
    Route::get('user','Api\UserAPIController@show');

// Route::resource('users', 'Api\UserAPIController', [
//     'only' => ['index', 'show', 'store', 'update', 'destroy']
// ]);
    
});