<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Links;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Links::class, function (Faker $faker) {
    return [
        'longurl' => 'https://en.wikipedia.org/wiki/B.o.B.',
        'shortcode' => Str::random(5),
        'user_id' => function ()
        {
            return factory(App\User::class)->create();
        }
    ];
});
