<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Links extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'longurl', 'shortcode', 'user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $appends = [
        'shorturl'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'longurl' => 'string',
        'shortcode' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    /**
     * Get the User for the Links.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function getShortUrlAttribute()
    {
        return url($this->shortcode);
    }

}
