<?php

namespace App\Http\Controllers\Api;

use App\Links;
use App\Http\Resources\LinksCollection;
use App\Http\Resources\LinksResource;
use App\Http\Controllers\Controller;
 
class LinksAPIController extends Controller
{
    public function index()
    {
        return new LinksCollection(Links::paginate());
    }
 
    public function show(Links $links)
    {
        return new LinksResource($links);
    }

    public function store(Request $request)
    {
        return new LinksResource(Links::create($request->all()));
    }

    public function update(Request $request, Links $links)
    {
        $links->update($request->all());

        return new LinksResource($links);
    }

    public function destroy(Request $request, Links $links)
    {
        $links->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }
}
