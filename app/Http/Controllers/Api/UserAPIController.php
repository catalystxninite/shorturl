<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserAPIController extends Controller
{
    public function index()
    {
        return new UserCollection(User::paginate());
    }
 
    public function show(User $user)
    {
        if($user)
        {
            $user = Auth::user();
        }
        return new UserResource($user->load(['links']));
    }

    public function store(Request $request)
    {
        return new UserResource(User::create($request->all()));
    }

    public function update(Request $request, User $user)
    {
        $user->update($request->all());

        return new UserResource($user);
    }

    public function destroy(Request $request, User $user)
    {
        $user->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }
}
