<?php

namespace App\Http\Controllers;

use App\Links;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Request;

class LinksController extends BaseController
{
    public function show($links)
    {
        $link = Links::where('shortcode',$links)->first();
        return redirect()->to($link->longurl,301);
    }
}